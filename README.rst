==============
gitlab cleaner
==============

This repository provides a simple tool called ``gitlab-cleaner`` which aims
to clean various GitLab artifacts (currently only pipelines) that cannot be
done using GitLab's web UI in bulk.

Prerequisites
=============
For software prerequisites, please follow with:

::

    $ pip3 install --user -r requirements.txt

OR

::

    $ python3 -m venv <path_to_venv>
    $ source <path_to_venv>/bin/activate
    $ pip install -r requirements.txt

Other than that you'll need to create a personal access token with full API
access in GitLab's web UI. The token is only displayed once, so save it
somewhere safe.

First steps
===========

Since exposing the API token (with full access rights) in Bash history would be
a security risk, you can supply your token either via the ``GITLAB_TOKEN``
environment variable or with the ``--token-from-file`` CLI option.
Next you'll need either the namespace ID (a group or your personal namespace ID
depending on what scale you want to perform the cleanup) or specifically the
project ID that you want to work with. Don't forget to set the right
permissions to the token - it needs full API access and if creating a
group/project access token, it also needs "owner" rights.


Examples
========

Whenever you want clean anything, first run the command with ``--dryrun`` to
verify that the results match the expectations!

To list all pipelines that would be cleaned up in project ``ID:123456``, run

::

    $ ./gitlab_cleaner.py -P 123456 -f tokenfile pipelines --dryrun

To clean pipelines in project ``ID:123456`` older than 30 days, run

::

    $ ./gitlab_cleaner.py -P 123456 -f tokenfile pipelines --days 30

To clean pipelines older than e.g. ``July 15th 2022`` in **all**
projects under the group namespace ``ID:456789``:

::

    $ ./gitlab_cleaner.py \
        -G 456789 \
        -f tokenfile \
        pipelines \
            --before "$(date -d '15 Jul 2022' --iso-8601)"

which will iterate over all projects in the group namespace and their respective
pipelines and remove the filtered subset. In order to silence the logging,
either set a lower log level:

::

    $ ./gitlab_cleaner.py \
        --log-level ERROR \
        -G 456789 \
        -f tokenfile \
        pipelines \
            --before "$(date -d '15 Jul 2022' --iso-8601)"

or simply use ``--quiet`` (won't print any log output):

::

    $ ./gitlab_cleaner.py \
        --quiet \
        -G 456789 \
        -f tokenfile \
        pipelines \
            --before "$(date -d '15 Jul 2022' --iso-8601)"

Same cleaning can be done on pipeline jobs with the exception that jobs are
actually not deleted, only their artifacts (job log, file artifacts) are erased
which is enough if you want to keep some statistics on pipelines, but still
be under GitLab-emposed storage quota. Running

::

    $ ./gitlab_cleaner.py \
        -P 456789 \
        -f tokenfile \
        jobs \
            --days 14

will erase all jobs older than 14 days.
