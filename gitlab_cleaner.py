#!/usr/bin/env python3

import gitlab  # needs pip install python-gitlab first!

import abc
import argparse
import datetime as dt
import logging
import os
import sys

from itertools import chain

log = None


class GitlabObjectAbstract(abc.ABC):
    @abc.abstractmethod
    def clean(self, *args, **kwargs):
        pass

    @abc.abstractstaticmethod
    def _list_proj_objs(proj, before_date):
        pass

    @staticmethod
    def _gitlab_list_objs(obj, **kwargs):
        kwargs.setdefault("get_all", True)
        kwargs.setdefault("iterator", True)
        return obj.list(**kwargs)

    @staticmethod
    def _gitlab_delete_obj(obj):
        obj.delete()


class GitlabObject(GitlabObjectAbstract):

    def __init__(self, token, group_id=None, user_id=None, project_id=None):
        self._projects = []
        self._gl = gitlab.Gitlab(private_token=token)

        if project_id:
            self._projects = [self._gl.projects.get(id=project_id)]
        else:
            try:
                if group_id:
                    _id = group_id
                    ns = self._gl.groups.get(id=group_id)
                elif user_id:
                    _id = user_id
                    ns = self._gl.users.get(id=user_id)
                else:
                    log.error(f"Either project or group or user ID required")
                    sys.exit(1)
                self._projects = self._get_namespace_projects(ns)
            except gitlab.GitlabGetError as e:
                log.error(f"Failed to query namespace ID='{_id}': {e}")
                sys.exit(1)

    def _get_namespace_projects(self, ns):
        # The project objects we get from group/user namespaces are of type
        # gitlab.v4.objects.projects.GroupProject OR
        # gitlab.v4.objects.projects.UserProject respectively, but these types
        # don't have all the data we need, so we need to query a specific
        # object of type gitlab.v4.objects.projects.Project instead
        log.info("Scanning projects...")
        return [
            self._gl.projects.get(proj.id)
            for proj in self._gitlab_list_objs(ns.projects)
        ]

    def clean(self, before_date, dryrun=False):
        obj_name = self.__class__.__name__.lower()
        items = 0

        for proj in self._projects:
            if proj.archived:
                log.info(f"{proj.name} ({proj.id}): SKIP [archived]")
                continue

            for gitlabobj in self._list_proj_objs(proj, before_date):
                log.info((f"{proj.name} ({proj.id}): "
                          f"[{obj_name}]: DELETE "
                          f"{gitlabobj.id} created at "
                          f"{gitlabobj.attributes['created_at']}"))

                if dryrun:
                    continue

                try:
                    self._gitlab_delete_obj(gitlabobj)
                except gitlab.GitlabError as ex:
                    log.info((f"{proj.name} ({proj.id}): "
                              f"[{obj_name}]: SKIP {gitlabobj.id}: "
                              f"{ex.error_message}"))
                else:
                    items += 1
            log.info(f"{proj.name} ({proj.id}): DONE")
            return items


class Pipelines(GitlabObject):
    @staticmethod
    def _list_proj_objs(proj, before_date):
        log.info("Scanning pipelines...")
        try:
            return GitlabObject._gitlab_list_objs(proj.pipelines,
                                                  updated_before=before_date)
        except gitlab.GitlabListError as ex:
            if ex.response_code == 403:
                log.info(f"{proj.name} ({proj.id}: SKIP [CI disabled]")
                return []
            else:
                raise

    def clean(self, before_date, dryrun=False):
        # To truly remove *all* the desired entries, we'll need to do
        # more passes, because we'll only ever get an iterator object to the
        # pipelines (because the list can be huge).
        # However, as we'll be removing pipelines GitLab will be re-adjusting
        # the result pages with which the iterator object simply can't cope and
        # the result being that some pipelines simply wouldn't be deleted
        # (because GitLab moved them to an earlier page which the iterator
        # had already queried and passed onto us).
        #
        while super().clean(before_date, dryrun) > 0:
            pass


class Jobs(GitlabObject):
    @staticmethod
    def _gitlab_delete_obj(obj):
        obj.erase()

    @staticmethod
    def _list_proj_objs(proj, before_date):
        """ Returns an iterator object to jobs older than 'before' timespec """

        log.info("Scanning jobs...")
        try:
            jobs = []
            pipelines = GitlabObject._gitlab_list_objs(proj.pipelines,
                                                       updated_before=before_date)
            for pipeline in pipelines:
                pl_jobs = filter(lambda pl_job: pl_job.artifacts,
                                 GitlabObject._gitlab_list_objs(pipeline.jobs))
                jobs.append(map(lambda pl_job: proj.jobs.get(pl_job.id), pl_jobs))

                return chain.from_iterable(jobs)
        except gitlab.GitlabListError as ex:
            if ex.response_code == 403:
                log.info(f"{proj.name} ({proj.id}: SKIP [CI disabled]")
                return []
            else:
                raise


class GitlabObjectHandler:
    def __init__(self):
        self._handlers = {
            "pipelines": Pipelines,
            "jobs": Jobs,
        }

    def run(self, handler, token, group_id, user_id, project_id, before_date,
            dryrun):
        handler = self._handlers[handler]
        handler(token,
                user_id=user_id,
                group_id=group_id,
                project_id=project_id).clean(before_date=before_date,
                                             dryrun=dryrun)


def run(args):
    token = None
    user_id = None
    group_id = None
    project_id = None
    before = dt.datetime.utcnow().isoformat()
    if args.token_file:
        try:
            # the file has already been opened by argparse
            token = args.token_file.read().strip()
        except FileNotFoundError as e:
            log.error(e)
            sys.exit(1)

    if args.group_id:
        group_id = args.group_id

    if args.user_id:
        user_id = args.user_id

    if args.project_id:
        project_id = args.project_id

    # CLI options have precedence over env variables
    token = os.environ.get("GITLAB_TOKEN", token)
    group_id = os.environ.get("GITLAB_GROUP_ID", group_id)
    user_id = os.environ.get("GITLAB_USER_ID", user_id)
    project_id = os.environ.get("GITLAB_PROJECT_ID", project_id)

    if not token:
        log.error("Missing GitLab API token")
        sys.exit(1)

    if [bool(_id) for _id in [user_id, group_id, project_id]].count(True) > 1:
        log.error("Group ID, User ID, and project ID are mutually exclusive")
        sys.exit(1)

    if args.days:
        tdelta = dt.timedelta(days=args.days)
        before = (dt.datetime.utcnow() - tdelta).isoformat()

    if args.before:
        try:
            before = dt.datetime.fromisoformat(args.before)
        except ValueError as e:
            log.error(e)
            sys.exit(1)

    GitlabObjectHandler().run(args.obj,
                              dryrun=args.dryrun,
                              user_id=user_id,
                              group_id=group_id,
                              project_id=project_id,
                              token=token,
                              before_date=before)


def setup_logging(args):
    global log

    logging.basicConfig(level=logging._nameToLevel[args.log_level])
    formatter = logging.Formatter("[%(levelname)s]: %(message)s")
    log = logging.getLogger()
    if args.quiet:
        # Note that the 'disabled' attr is not public API, but does the job
        log.disabled = True
    handler = log.handlers[0]
    handler.setFormatter(formatter)


def argparse_check_positive(value):
    val = int(value)
    if val <= 0:
        raise argparse.ArgumentTypeError(f"{val} is not a positive integer")
    return val


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--log-level",
        type=str,
        choices=sorted(logging._nameToLevel.keys()),
        default="INFO",
        help="log level (default is INFO)",
    )

    parser.add_argument(
        "-q", "--quiet",
        action="store_true",
        default=False,
        help="silence all output",
    )

    parser.add_argument(
        "-f", "--token-from-file",
        dest="token_file",
        type=argparse.FileType("r"),
        help="file from which the API token should be read",
    )

    ns_group = parser.add_mutually_exclusive_group()
    ns_group.add_argument(
        "-G", "--group-id",
        dest="group_id",
        type=str,
        help=("GitLab group ID (will affect every project in the namespace)"),
    )

    ns_group.add_argument(
        "-U", "--user-id",
        type=str,
        dest="user_id",
        help="GitLab user ID (will affect every project in the namespace)",
    )

    ns_group.add_argument(
        "-P", "--project-id",
        type=str,
        dest="project_id",
        help="GitLab project ID",
    )

    subparsers = parser.add_subparsers(
        metavar="GITLAB_OBJECT",
        dest="obj",
    )
    subparsers.required = True

    common_parser = argparse.ArgumentParser(add_help=False)
    common_parser.add_argument(
        "-n", "--dryrun",
        dest="dryrun",
        action="store_true",
        default=False,
        help="only print the action, don't perform it",
    )

    ts_group = common_parser.add_mutually_exclusive_group(required=False)
    ts_group.add_argument(
        "--all",
        action="store_true",
        help="filter all objects (this is the default)"
    )

    ts_group.add_argument(
        "-d", "--days",
        type=argparse_check_positive,
        help="filter objects older than N days",
    )

    ts_group.add_argument(
        "-B", "--before",
        metavar="TIMESPEC",
        type=str,
        help="filter objects older than <ISO 8601 timespec>",
    )

    obj_parsers = {}
    for obj in ["pipelines", "jobs"]:
        obj_parsers[obj] = subparsers.add_parser(
            obj,
            help=f"operate over a set of {obj}",
            parents=[common_parser]
        )

    args = parser.parse_args()
    setup_logging(args)
    run(args)
